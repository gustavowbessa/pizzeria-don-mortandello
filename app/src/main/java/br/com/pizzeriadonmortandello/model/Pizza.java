package br.com.pizzeriadonmortandello.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;

public class Pizza implements Parcelable {

    private Integer id;
    private String nome;
    private String ingredientes;
    private Double precoM;
    private Double precoP;
    private Double precoG;

    public Pizza() {

    }

    public Pizza(Integer id, String nome, String ingredientes, Double precoM) {
        this.id = id;
        this.nome = nome;
        this.ingredientes = ingredientes;
        this.precoM = precoM;
        this.precoP = 0.25 * precoM;
        this.precoG = 1.25 * precoM;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(String ingredientes) {
        this.ingredientes = ingredientes;
    }

    public Double getPrecoP() {
        return precoP;
    }

    public Double getPrecoG() {
        return precoG;
    }

    public Double getPrecoM() {
        return precoM;
    }

    public void setPrecoM(Double precoM) {
        this.precoM = precoM;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String formatarPrecoMPizza() {
        String precoString = BigDecimal.valueOf(this.precoM).setScale(2).toString();
        return "R$ ".concat(precoString.replace(".", ","));
    }

    /* PARCELABLE */

    protected Pizza(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        nome = in.readString();
        ingredientes = in.readString();
        if (in.readByte() == 0) {
            precoM = null;
        } else {
            precoM = in.readDouble();
        }
        if (in.readByte() == 0) {
            precoP = null;
        } else {
            precoP = in.readDouble();
        }
        if (in.readByte() == 0) {
            precoG = null;
        } else {
            precoG = in.readDouble();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(nome);
        dest.writeString(ingredientes);
        if (precoM == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(precoM);
        }
        if (precoP == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(precoP);
        }
        if (precoG == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(precoG);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Pizza> CREATOR = new Creator<Pizza>() {
        @Override
        public Pizza createFromParcel(Parcel in) {
            return new Pizza(in);
        }

        @Override
        public Pizza[] newArray(int size) {
            return new Pizza[size];
        }
    };
}
