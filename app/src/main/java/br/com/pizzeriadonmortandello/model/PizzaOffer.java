package br.com.pizzeriadonmortandello.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class PizzaOffer implements Parcelable {
    private Integer id;
    private String nome;
    private ArrayList<Pizza> pizzas;
    private Integer discountPercent;

    public PizzaOffer(Integer id, String nome, ArrayList<Pizza> pizzas, Integer discountPercent) {
        this.id = id;
        this.nome = nome;
        this.pizzas = pizzas;
        this.discountPercent = discountPercent;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ArrayList<Pizza> getPizzas() {
        return pizzas;
    }

    public void setPizzas(ArrayList<Pizza> pizzas) {
        this.pizzas = pizzas;
    }

    public Integer getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(Integer discountPercent) {
        this.discountPercent = discountPercent;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    /* PARCELABLE */

    protected PizzaOffer(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        nome = in.readString();
        pizzas = in.createTypedArrayList(Pizza.CREATOR);
        if (in.readByte() == 0) {
            discountPercent = null;
        } else {
            discountPercent = in.readInt();
        }
    }

    public static final Creator<PizzaOffer> CREATOR = new Creator<PizzaOffer>() {
        @Override
        public PizzaOffer createFromParcel(Parcel in) {
            return new PizzaOffer(in);
        }

        @Override
        public PizzaOffer[] newArray(int size) {
            return new PizzaOffer[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(nome);
        dest.writeTypedList(pizzas);
        if (discountPercent == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(discountPercent);
        }
    }
}
