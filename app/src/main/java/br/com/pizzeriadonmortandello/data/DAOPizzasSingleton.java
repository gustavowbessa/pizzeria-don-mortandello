package br.com.pizzeriadonmortandello.data;

import java.util.ArrayList;

import br.com.pizzeriadonmortandello.model.Pizza;

public class DAOPizzasSingleton {

    private ArrayList<Pizza> pizzas;
    private static DAOPizzasSingleton INSTANCE;

    private DAOPizzasSingleton() {

    }

    public ArrayList<Pizza> getPizzas() {
        if (this.pizzas == null) {
            this.pizzas = new ArrayList<>();
        }
        return this.pizzas;
    }

    public void addPizza(Pizza pizza) {
        if (this.pizzas == null) {
            this.pizzas = new ArrayList<>();
        }
        this.pizzas.add(pizza);
    }

    public static DAOPizzasSingleton getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new DAOPizzasSingleton();
        }
        return INSTANCE;
    }

}
