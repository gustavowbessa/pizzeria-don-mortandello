package br.com.pizzeriadonmortandello.data;

import java.util.ArrayList;

import br.com.pizzeriadonmortandello.model.PizzaOffer;

public class DAOPizzaOffersSingleton {

    private ArrayList<PizzaOffer> pizzaOffers;
    private static DAOPizzaOffersSingleton INSTANCE;

    private DAOPizzaOffersSingleton() {

    }

    public ArrayList<PizzaOffer> getPizzaOffers() {
        if (this.pizzaOffers == null) {
            this.pizzaOffers = new ArrayList<>();
        }
        return this.pizzaOffers;
    }

    public void addPizzaOffer(PizzaOffer pizzaOffer) {
        if (this.pizzaOffers == null) {
            this.pizzaOffers = new ArrayList<>();
        }
        this.pizzaOffers.add(pizzaOffer);
    }

    public static DAOPizzaOffersSingleton getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new DAOPizzaOffersSingleton();
        }
        return INSTANCE;
    }
}
