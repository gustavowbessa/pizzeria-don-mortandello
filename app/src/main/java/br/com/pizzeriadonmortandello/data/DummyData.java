package br.com.pizzeriadonmortandello.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import br.com.pizzeriadonmortandello.model.Pizza;
import br.com.pizzeriadonmortandello.model.PizzaOffer;

public class DummyData {
    public DummyData() {
        this.createDummyPizzas();
        this.createDummyPizzaOffers();
    }

    private void createDummyPizzas() {
        DAOPizzasSingleton.getINSTANCE().addPizza(new Pizza(1, "Calabresa", "Muçarela, Orégano, Azeitona, Cebola, Calabresa e Tomate", 45.0));
        DAOPizzasSingleton.getINSTANCE().addPizza(new Pizza(2, "Prestígio", "Creme de Coco, Chocolate ao Leite e Cereja", 53.0));
        DAOPizzasSingleton.getINSTANCE().addPizza(new Pizza(3, "Muçarela", "Muçarela, Orégano, Azeitona e Tomate", 45.0));
        DAOPizzasSingleton.getINSTANCE().addPizza(new Pizza(4, "Presunto", "Muçarela, Orégano, Azeitona, Cebola, Presunto e Tomate", 45.0));
        DAOPizzasSingleton.getINSTANCE().addPizza(new Pizza(5, "Creme de Milho", "Muçarela, Orégano, Azeitona, Creme de Milho, Milho Cozido, Parmesão e Tomate", 45.0));
        DAOPizzasSingleton.getINSTANCE().addPizza(new Pizza(6, "Atum", "Muçarela, Orégano, Azeitona, Cebola, Atum, Palmito e Tomate", 51.0));
        DAOPizzasSingleton.getINSTANCE().addPizza(new Pizza(7, "Aliche (Peixe)", "Muçarela, Orégano, Azeitona, Aliche e Tomate", 51.0));
        DAOPizzasSingleton.getINSTANCE().addPizza(new Pizza(8, "Palmito", "Muçarela, Orégano, Azeitona, Cebola, Palmito e Tomate", 52.0));
        DAOPizzasSingleton.getINSTANCE().addPizza(new Pizza(9, "Quatro Queijos", "Muçarela, Orégano, Azeitona, Parmesão, Gorgonzola, Catupiry e Tomate", 53.0));
        DAOPizzasSingleton.getINSTANCE().addPizza(new Pizza(10, "Frango com Catupiry", "Muçarela, Orégano, Azeitona, Filé de Peito de Frango, Catupiry e Molho ao Sugo", 52.0));
        DAOPizzasSingleton.getINSTANCE().addPizza(new Pizza(11, "Portuguesa", "Muçarela, Orégano, Azeitona, Cebola, Presunto, Ervilha, Palmito, Calabresa, Ovo e Tomate", 53.0));
        DAOPizzasSingleton.getINSTANCE().addPizza(new Pizza(12,"Parmegiana", "Muçarela, Orégano, Azeitona, Presunto, Parmesão, Tomate, Bacon e Molho ao Sugo", 51.0));
        DAOPizzasSingleton.getINSTANCE().addPizza(new Pizza(13, "Calabresa com Catupiry", "Muçarela, Orégano, Azeitona, Cebola, Calabresa, Catupiry e Tomate", 49.0));
    }

    private void createDummyPizzaOffers() {
        int numPromos = 5;
        for (int i = 0; i < numPromos; i++) {
            DAOPizzaOffersSingleton.getINSTANCE().getPizzaOffers().add(generatePizzaOffer(i));
        }
    }

    private PizzaOffer generatePizzaOffer(int id) {
        Random aleatorio = new Random();
        int numMaxPizzaPromo = 3;
        int minPct = 5;
        int maxPct = 20;
        // Incrementar para ser possível pegar o último valor do intervalo
        numMaxPizzaPromo++;
        maxPct++;
        int numPizzaOferta = aleatorio.nextInt(numMaxPizzaPromo - 1) + 1;
        int discountPercent = aleatorio.nextInt(maxPct - minPct) + minPct;
        ArrayList<Pizza> pizzasInOffer = new ArrayList<>();
        for (int i = 0; i < numPizzaOferta; i++) {
            int randomIndex = aleatorio.nextInt(DAOPizzasSingleton.getINSTANCE().getPizzas().size());
            pizzasInOffer.add(DAOPizzasSingleton.getINSTANCE().getPizzas().get(randomIndex));
        }
        String nome = "PROMO ".concat(String.valueOf(id + 1));
        return new PizzaOffer(id, nome, pizzasInOffer, discountPercent);
    }
}
