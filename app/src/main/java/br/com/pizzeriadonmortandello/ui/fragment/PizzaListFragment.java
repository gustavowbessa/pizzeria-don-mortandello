package br.com.pizzeriadonmortandello.ui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import br.com.pizzeriadonmortandello.R;
import br.com.pizzeriadonmortandello.data.DAOPizzasSingleton;
import br.com.pizzeriadonmortandello.model.Pizza;
import br.com.pizzeriadonmortandello.ui.list_builders.PizzaListBuilder;

public class PizzaListFragment extends Fragment {

    public static final String TAG = "PizzaListFragment";
    public static final String PIZZA_LIST_KEY = "PizzaListFragment.PIZZA_LIST_KEY";

    public PizzaListFragment() {
        super(R.layout.pizza_list_fragment);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ArrayList<Pizza> pizzas = new ArrayList<>();
        if (getArguments() != null) {
            pizzas = getArguments().getParcelableArrayList(this.PIZZA_LIST_KEY);
        }
        new PizzaListBuilder(getActivity(), R.id.pizza_list)
                .load(pizzas, getActivity());
    }
}
