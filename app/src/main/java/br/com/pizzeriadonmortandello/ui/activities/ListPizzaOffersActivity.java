package br.com.pizzeriadonmortandello.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import br.com.pizzeriadonmortandello.R;
import br.com.pizzeriadonmortandello.ui.fragment.PizzaOfferListFragment;

public class ListPizzaOffersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizza_offers);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.offersFcv, PizzaOfferListFragment.class, savedInstanceState,
                        PizzaOfferListFragment.TAG).commit();
    }
}