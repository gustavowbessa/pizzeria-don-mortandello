package br.com.pizzeriadonmortandello.ui.list_builders;

import android.app.Activity;

import androidx.annotation.IdRes;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import br.com.pizzeriadonmortandello.model.Pizza;
import br.com.pizzeriadonmortandello.ui.list.pizza.PizzaDataAdapter;


public class PizzaListBuilder {

    private RecyclerView rvPizzaList;
    private LinearLayoutManager layoutManager;
    private PizzaDataAdapter adapter;

    public PizzaListBuilder(Activity activity, @IdRes int rvPizzas) {
        this.rvPizzaList = activity.findViewById(rvPizzas);
        this.layoutManager = new LinearLayoutManager(activity);
        this.rvPizzaList.setLayoutManager(this.layoutManager);
        this.adapter = null;
    }

    public PizzaListBuilder load(ArrayList<Pizza> pizzas, Activity activity) {
        this.adapter = new PizzaDataAdapter(pizzas, activity);
        this.rvPizzaList.setAdapter(this.adapter);
        return this;
    }
}
