package br.com.pizzeriadonmortandello.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.math.BigDecimal;

import br.com.pizzeriadonmortandello.R;
import br.com.pizzeriadonmortandello.model.Pizza;
import br.com.pizzeriadonmortandello.model.PizzaOffer;
import br.com.pizzeriadonmortandello.ui.fragment.PizzaListFragment;
import br.com.pizzeriadonmortandello.ui.fragment.PizzaOfferListFragment;

public class PizzaOfferDetailsActivity extends AppCompatActivity {

    public static String KEY = "PizzaOfferDetailsActivity.PIZZA_OFFER";
    private PizzaOffer pizzaOffer;
    private TextView txtPizzaOfferName;
    private TextView txtPizzaOfferDiscount;
    private TextView txtPizzaOfferTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizza_offer_details);

        if (getIntent() != null) {
            pizzaOffer = getIntent().getParcelableExtra(this.KEY);
        }

        Double sum = this.pizzaOffer.getPizzas().stream().map(pizza -> pizza.getPrecoM()).reduce(0D, Double::sum);
        String precoString = BigDecimal.valueOf(sum).setScale(2).toString();
        precoString = "R$ ".concat(precoString.replace(".", ","));

        this.txtPizzaOfferName = findViewById(R.id.pizza_offer_detail_name);
        this.txtPizzaOfferDiscount = findViewById(R.id.pizza_offer_detail_discount);
        this.txtPizzaOfferTotal = findViewById(R.id.pizza_offer_detail_total);

        this.txtPizzaOfferName.setText(pizzaOffer.getNome());
        this.txtPizzaOfferDiscount.setText(pizzaOffer.getDiscountPercent().toString().concat("%"));
        this.txtPizzaOfferTotal.setText(precoString);

        PizzaListFragment fragment = new PizzaListFragment();
        Bundle args = new Bundle();

        args.putParcelableArrayList(PizzaListFragment.PIZZA_LIST_KEY, pizzaOffer.getPizzas());
        fragment.setArguments(args);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.cardapioOfertaFcv, fragment, PizzaListFragment.TAG)
                .commit();
    }
}