package br.com.pizzeriadonmortandello.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import br.com.pizzeriadonmortandello.R;
import br.com.pizzeriadonmortandello.data.DAOPizzasSingleton;
import br.com.pizzeriadonmortandello.data.DummyData;
import br.com.pizzeriadonmortandello.model.Pizza;

public class MainActivity extends AppCompatActivity {
    private static final int FORM_REQUEST_CODE = 201;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Instanciar Dados Fake se estiver vazia
        if (DAOPizzasSingleton.getINSTANCE().getPizzas().size() == 0) {
            new DummyData();
        }

        btn = findViewById(R.id.btn_create_pizza);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, NewPizzaActivity.class);
                startActivityForResult(i, FORM_REQUEST_CODE);
            }
        });

        btn = findViewById(R.id.btn_pizza_menu);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListPizzasActivity.class);
                startActivity(i);
            }
        });

        btn = findViewById(R.id.btn_pizza_offers);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListPizzaOffersActivity.class);
                startActivity(i);
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == FORM_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            /* Pegar pizza e salvar */
            Pizza pizza = data.getParcelableExtra(NewPizzaActivity.RESULT_KEY);
            DAOPizzasSingleton.getINSTANCE().addPizza(pizza);
        }
    }
}