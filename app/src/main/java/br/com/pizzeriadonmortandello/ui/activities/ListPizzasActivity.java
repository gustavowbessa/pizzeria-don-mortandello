package br.com.pizzeriadonmortandello.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import br.com.pizzeriadonmortandello.R;
import br.com.pizzeriadonmortandello.data.DAOPizzasSingleton;
import br.com.pizzeriadonmortandello.ui.fragment.PizzaListFragment;

public class ListPizzasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_pizzas);

        PizzaListFragment fragment = new PizzaListFragment();
        Bundle args = new Bundle();

        args.putParcelableArrayList(PizzaListFragment.PIZZA_LIST_KEY, DAOPizzasSingleton.getINSTANCE().getPizzas());
        fragment.setArguments(args);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.cardapioFcv, fragment, PizzaListFragment.TAG)
                .commit();
    }
}