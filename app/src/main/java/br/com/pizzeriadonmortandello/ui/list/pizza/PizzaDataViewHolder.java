package br.com.pizzeriadonmortandello.ui.list.pizza;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.math.BigDecimal;

import br.com.pizzeriadonmortandello.R;
import br.com.pizzeriadonmortandello.model.Pizza;
import br.com.pizzeriadonmortandello.ui.components.CustomDialog;

public class PizzaDataViewHolder extends RecyclerView.ViewHolder
                                    implements View.OnClickListener {

    private Pizza pizzaAtual;
    private final TextView txtNomePizza;
    private final TextView txtPrecoPizza;
    private final TextView txtIngredientesPizza;
    private final PizzaDataAdapter adapter;

    public PizzaDataViewHolder(@NonNull View itemView, PizzaDataAdapter adapter) {
        super(itemView);
        this.txtNomePizza = itemView.findViewById(R.id.item_pizza_name);
        this.txtPrecoPizza = itemView.findViewById(R.id.item_pizza_price);
        this.txtIngredientesPizza = itemView.findViewById(R.id.item_pizza_ingredients);
        itemView.setOnClickListener(this);
        txtPrecoPizza.setOnClickListener(this);
        this.adapter = adapter;
    }

    public void bind(Pizza pizza) {
        this.pizzaAtual = pizza;
        this.txtNomePizza.setText(pizza.getNome());
        this.txtPrecoPizza.setText(pizza.formatarPrecoMPizza());
        this.txtIngredientesPizza.setText(pizza.getIngredientes());
        this.updateOpenView();
    }

    private void updateOpenView() {
        boolean isOpen = adapter.getOpenViewCacheFor(this.pizzaAtual.getId());
        this.txtIngredientesPizza.setVisibility(isOpen ? View.VISIBLE : View.GONE);
    }

    private void toggleDescription() {
        boolean isVisible = this.txtIngredientesPizza.getVisibility() == View.VISIBLE;
        int visibility;
        if(isVisible) {
            visibility = View.GONE;
            this.adapter.unsetOpenViewCacheFor(this.pizzaAtual.getId());
        }
        else {
            visibility = View.VISIBLE;
            this.adapter.setOpenViewCacheFor(this.pizzaAtual.getId());
        }
        this.txtIngredientesPizza.setVisibility(visibility);
    }

    public void showPizzaInfo(Pizza pizza) {
        CustomDialog dialog = new CustomDialog(this.adapter.getActivity(), pizza);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        if (itemView == v) {
            this.toggleDescription();
        }
        else {
            this.showPizzaInfo(this.pizzaAtual);
        }
    }
}
