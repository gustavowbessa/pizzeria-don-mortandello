package br.com.pizzeriadonmortandello.ui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import br.com.pizzeriadonmortandello.R;
import br.com.pizzeriadonmortandello.data.DAOPizzaOffersSingleton;
import br.com.pizzeriadonmortandello.ui.list_builders.PizzaOfferListBuilder;

public class PizzaOfferListFragment extends Fragment {

    public static final String TAG = "PizzaOfferListFragment";

    public PizzaOfferListFragment() {
        super(R.layout.pizza_offer_list_fragment);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new PizzaOfferListBuilder(getActivity(), R.id.pizza_offer_list)
                .load(DAOPizzaOffersSingleton.getINSTANCE().getPizzaOffers(), getActivity());
    }
}
