package br.com.pizzeriadonmortandello.ui.components;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import java.math.BigDecimal;

import br.com.pizzeriadonmortandello.R;
import br.com.pizzeriadonmortandello.model.Pizza;

public class CustomDialog extends Dialog implements
        android.view.View.OnClickListener {

    public Activity activity;
    public Button btn;
    private Pizza pizza;

    public CustomDialog(Activity parentActivity, Pizza pizza) {
        super(parentActivity);
        this.activity = parentActivity;
        this.pizza = pizza;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.pizza_dialog);
        btn = (Button) findViewById(R.id.btn_ok);
        btn.setOnClickListener(this);
        ((TextView) findViewById(R.id.dialog_pizza_name)).setText(pizza.getNome());
        ((TextView) findViewById(R.id.dialog_pizza_ingredients)).setText(pizza.getIngredientes());
        ((TextView) findViewById(R.id.pizzaP)).setText("R$".concat(BigDecimal.valueOf(pizza.getPrecoP()).setScale(2, BigDecimal.ROUND_HALF_UP).toString()));
        ((TextView) findViewById(R.id.pizzaM)).setText("R$".concat(BigDecimal.valueOf(pizza.getPrecoM()).setScale(2, BigDecimal.ROUND_HALF_UP).toString()));
        ((TextView) findViewById(R.id.pizzaG)).setText("R$".concat(BigDecimal.valueOf(pizza.getPrecoG()).setScale(2, BigDecimal.ROUND_HALF_UP).toString()));
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
}
