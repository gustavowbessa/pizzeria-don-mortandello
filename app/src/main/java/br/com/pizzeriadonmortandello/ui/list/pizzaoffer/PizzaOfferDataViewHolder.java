package br.com.pizzeriadonmortandello.ui.list.pizzaoffer;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import br.com.pizzeriadonmortandello.R;
import br.com.pizzeriadonmortandello.model.Pizza;
import br.com.pizzeriadonmortandello.model.PizzaOffer;
import br.com.pizzeriadonmortandello.ui.activities.PizzaOfferDetailsActivity;

public class PizzaOfferDataViewHolder extends RecyclerView.ViewHolder
                                    implements View.OnClickListener {

    private PizzaOffer pizzaOfferAtual;
    private final TextView txtOfferName;
    private final ImageView eyeImage;
    private final TextView txtOfferDescription;
    private final PizzaOfferDataAdapter adapter;

    public PizzaOfferDataViewHolder(@NonNull View itemView, PizzaOfferDataAdapter adapter) {
        super(itemView);
        this.txtOfferName = itemView.findViewById(R.id.item_pizza_offer_name);
        this.eyeImage = itemView.findViewById(R.id.eye_image);
        this.txtOfferDescription = itemView.findViewById(R.id.offer_pizza_list);
        itemView.setOnClickListener(this);
        eyeImage.setOnClickListener(this);
        this.adapter = adapter;
    }

    public void bind(PizzaOffer pizzaOffer) {
        this.pizzaOfferAtual = pizzaOffer;
        this.txtOfferName.setText(pizzaOffer.getNome());
        this.txtOfferDescription.setText("PIZZAS (".concat(pizzaOffer.getDiscountPercent().toString()).concat("%)\n"));
        for (Pizza pizza : pizzaOffer.getPizzas()) {
            this.txtOfferDescription.setText(this.txtOfferDescription.getText().toString().concat("\n".concat(pizza.getNome())));
        }
        this.updateOpenView();
    }

    private void updateOpenView() {
        boolean isOpen = adapter.getOpenViewCacheFor(this.pizzaOfferAtual.getId());
        this.txtOfferDescription.setVisibility(isOpen ? View.VISIBLE : View.GONE);
    }

    private void toggleDescription() {
        boolean isVisible = this.txtOfferDescription.getVisibility() == View.VISIBLE;
        int visibility;
        if(isVisible) {
            visibility = View.GONE;
            this.adapter.unsetOpenViewCacheFor(this.pizzaOfferAtual.getId());
        }
        else {
            visibility = View.VISIBLE;
            this.adapter.setOpenViewCacheFor(this.pizzaOfferAtual.getId());
        }
        this.txtOfferDescription.setVisibility(visibility);
    }

    public void showPizzaInfo() {
        Intent intent = new Intent(this.adapter.getActivity(), PizzaOfferDetailsActivity.class);
        intent.putExtra(PizzaOfferDetailsActivity.KEY, this.pizzaOfferAtual);
        this.adapter.getActivity().startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        if (itemView == v) {
            this.toggleDescription();
        }
        else {
            this.showPizzaInfo();
        }
    }
}
