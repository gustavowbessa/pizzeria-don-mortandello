package br.com.pizzeriadonmortandello.ui.list.pizza;

import android.app.Activity;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import br.com.pizzeriadonmortandello.R;
import br.com.pizzeriadonmortandello.model.Pizza;

public class PizzaDataAdapter extends RecyclerView.Adapter<PizzaDataViewHolder> {

    private ArrayList<Pizza> pizzas;
    private SparseBooleanArray toggleInfo;
    private Activity activity;

    public PizzaDataAdapter(ArrayList<Pizza> pizzas, Activity activity) {
        this.pizzas = pizzas;
        this.pizzas.sort((pizza1, pizza2) -> pizza1.getNome().compareTo(pizza2.getNome()));
        this.activity = activity;
        this.toggleInfo = new SparseBooleanArray();
    }

    public Activity getActivity() {
        return this.activity;
    }

    @NonNull
    @Override
    public PizzaDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.pizza_item_view, parent, false);
        return new PizzaDataViewHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(@NonNull PizzaDataViewHolder holder, int position) {
        holder.bind(this.pizzas.get(position));
    }

    @Override
    public int getItemCount() {
        return this.pizzas.size();
    }

    //----------- Métodos auxiliares

    public void setOpenViewCacheFor(int id) {
        this.toggleInfo.put(id, true);
    }

    public void unsetOpenViewCacheFor(int id) {
        this.toggleInfo.delete(id);
    }

    public boolean getOpenViewCacheFor(int id) {
        return this.toggleInfo.get(id, false);
    }
}
