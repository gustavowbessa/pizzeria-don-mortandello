package br.com.pizzeriadonmortandello.ui.list_builders;

import android.app.Activity;

import androidx.annotation.IdRes;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import br.com.pizzeriadonmortandello.model.Pizza;
import br.com.pizzeriadonmortandello.model.PizzaOffer;
import br.com.pizzeriadonmortandello.ui.list.pizza.PizzaDataAdapter;
import br.com.pizzeriadonmortandello.ui.list.pizzaoffer.PizzaOfferDataAdapter;


public class PizzaOfferListBuilder {

    private RecyclerView rvPizzaOfferList;
    private LinearLayoutManager layoutManager;
    private PizzaOfferDataAdapter adapter;

    public PizzaOfferListBuilder(Activity activity, @IdRes int rvPizzaOffers) {
        this.rvPizzaOfferList = activity.findViewById(rvPizzaOffers);
        this.layoutManager = new LinearLayoutManager(activity);
        this.rvPizzaOfferList.setLayoutManager(this.layoutManager);
        this.adapter = null;
    }

    public PizzaOfferListBuilder load(ArrayList<PizzaOffer> pizzaOffers, Activity activity) {
        this.adapter = new PizzaOfferDataAdapter(pizzaOffers, activity);
        this.rvPizzaOfferList.setAdapter(this.adapter);
        return this;
    }
}
