package br.com.pizzeriadonmortandello.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import br.com.pizzeriadonmortandello.R;
import br.com.pizzeriadonmortandello.data.DAOPizzasSingleton;
import br.com.pizzeriadonmortandello.model.Pizza;

public class NewPizzaActivity extends AppCompatActivity {

    // Result Key
    public static final String RESULT_KEY = "NewPizzaActivity.RESULT_KEY";

    // View references
    private Button btn;
    private EditText inputName;
    private EditText inputIngredients;
    private EditText inputPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_pizza);

        btn = findViewById(R.id.btn_save);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savePizza();
            }
        });
    }

    public void savePizza() {
        /* Find views */
        inputName = findViewById(R.id.pizza_name);
        inputIngredients = findViewById(R.id.pizza_ingredients);
        inputPrice = findViewById(R.id.pizza_price);

        /* Get values */
        String pizzaName = inputName.getText().toString();
        String pizzaIngredients = inputIngredients.getText().toString();
        Double pizzaPrice;
        try {
            pizzaPrice = Double.valueOf(inputPrice.getText().toString());
        } catch (Exception e) {
            View contextView = findViewById(R.id.layout);
            Snackbar.make(contextView, "Preço no formato incorreto. (Ex: 15.00)", Snackbar.LENGTH_SHORT)
                    .setTextColor(getResources().getColor(R.color.white))
                    .setBackgroundTint(getResources().getColor(R.color.grey_red))
                    .show();
            return;
        }

        /* Create pizza */
        Pizza pizza = new Pizza(1, pizzaName, pizzaIngredients, pizzaPrice);

        /* Colocar a pizza criada no result */
        Intent output = new Intent();
        output.putExtra(RESULT_KEY, pizza);
        setResult(RESULT_OK, output);

        /* Encerrar a intent */
        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}