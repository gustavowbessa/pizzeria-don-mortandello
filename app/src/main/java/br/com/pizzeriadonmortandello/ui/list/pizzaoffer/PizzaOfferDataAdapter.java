package br.com.pizzeriadonmortandello.ui.list.pizzaoffer;

import android.app.Activity;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import br.com.pizzeriadonmortandello.R;
import br.com.pizzeriadonmortandello.model.Pizza;
import br.com.pizzeriadonmortandello.model.PizzaOffer;

public class PizzaOfferDataAdapter extends RecyclerView.Adapter<PizzaOfferDataViewHolder> {

    private ArrayList<PizzaOffer> pizzaOffers;
    private SparseBooleanArray toggleInfo;
    private Activity activity;

    public PizzaOfferDataAdapter(ArrayList<PizzaOffer> pizzaOffers, Activity activity) {
        this.pizzaOffers = pizzaOffers;
        this.pizzaOffers.sort((pizzaOffer1, pizzaOffer2) -> pizzaOffer1.getNome().compareTo(pizzaOffer2.getNome()));
        this.activity = activity;
        this.toggleInfo = new SparseBooleanArray();
    }

    public Activity getActivity() {
        return this.activity;
    }

    @NonNull
    @Override
    public PizzaOfferDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.pizza_offer_item_view, parent, false);
        return new PizzaOfferDataViewHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(@NonNull PizzaOfferDataViewHolder holder, int position) {
        holder.bind(this.pizzaOffers.get(position));
    }

    @Override
    public int getItemCount() {
        return this.pizzaOffers.size();
    }

    //----------- Métodos auxiliares

    public void setOpenViewCacheFor(int id) {
        this.toggleInfo.put(id, true);
    }

    public void unsetOpenViewCacheFor(int id) {
        this.toggleInfo.delete(id);
    }

    public boolean getOpenViewCacheFor(int id) {
        return this.toggleInfo.get(id, false);
    }
}
